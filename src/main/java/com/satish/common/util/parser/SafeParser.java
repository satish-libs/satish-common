package com.satish.common.util.parser;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class SafeParser {

    private SafeParser() {
    }

    public static int tryParseInt(String value) {
        try {
            if (value == null) {
                return 0;
            }
            return Integer.parseInt(value);
        } catch (NumberFormatException nfe) {
            throw new NumberFormatException();
        }
    }

    public static double tryParseDouble(String value) {
        try {
            return parseDouble(value);
        } catch (NumberFormatException nfe) {
            throw new NumberFormatException();
        }
    }


    public static double tryParseDouble(String value, String message) {
        try {
            return parseDouble(value);
        } catch (NumberFormatException nfe) {
            throw new NumberFormatException(message);
        }
    }


    public static boolean parseBoolean(String value) {
        if (value == null) {
            return false;
        }
        return Boolean.parseBoolean(value);
    }


    public static <T> String safeParseToString(T value) {
        if (value == null) {
            return null;
        }
        switch (value.getClass().getSimpleName()) {
            case "Byte":
                return Byte.toString((Byte) value);
            case "Short":
                return Short.toString((Short) value);
            case "Character":
                return Character.toString((Character) value);
            case "Long":
                return Long.toString((Long) value);
            case "Boolean":
                return Boolean.toString((Boolean) value);
            case "Integer":
                return Integer.toString((Integer) value);
            case "Double":
                return Double.toString((Double) value);
            case "Float":
                return Float.toString((Float) value);
            default:
                return value.toString();
        }
    }

    public static LocalDate toLocalDate(String value) {
        if (value == null) {
            return null;
        }
        try {
            return LocalDate.parse(value, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        } catch (IllegalArgumentException lfe) {
            throw new IllegalArgumentException();
        }
    }

    private static double parseDouble(String value) {
        if (value == null) {
            return 0;
        }
        return Double.parseDouble(value);
    }
}
