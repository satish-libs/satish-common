package com.satish.common.util.enums;

public enum ValidatorType {
    PATTERN, MIN_LENGTH, MAX_LENGTH, MAX_VALUE, MIN_VALUE, REQUIRED
}
