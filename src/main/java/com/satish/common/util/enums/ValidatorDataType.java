package com.satish.common.util.enums;

public enum ValidatorDataType {
    INTEGER, DECIMAL, STRING, BOOLEAN
}
