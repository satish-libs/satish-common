package com.satish.common.util.numberutil;

public final class NumberUtil {

    public static boolean isNumeric(String value) {
        if (value == null) {
            return false;
        }
        try {
            Double.parseDouble(value.trim());
        } catch (NumberFormatException ne) {
            return false;
        }
        return true;
    }
}
