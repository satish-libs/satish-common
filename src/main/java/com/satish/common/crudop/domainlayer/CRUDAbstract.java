package com.satish.common.crudop.domainlayer;


import java.util.List;
import java.util.Set;


public abstract class CRUDAbstract<D, I> implements CRUDPort<D, I> {

    private final RepositoryPort<D, I> repository;

    public CRUDAbstract(final RepositoryPort<D, I> repository) {
        this.repository = repository;
    }

    @Override
    public D findById(I id) {
        return repository.findById(id);
    }

    @Override
    public List<D> findAll() {
        return repository.findAll();
    }

    @Override
    public D save(D t) {
        return repository.save(t);
    }

    @Override
    public D softDelete(I id) {
        return repository.softDelete(id);
    }

    @Override
    public D updateColumnBy(I id, String columnName, String value) {
        return repository.updateColumnBy(id, columnName, value);
    }

    @Override
    public Set<D> setOfAll() {
        return repository.setOfAll();
    }
}
