package com.satish.common.crudop.domainlayer;


public interface CRUDPort<T, I> extends RetrievalPort<T, I> {
    T save(T t);

    T softDelete(I id);

    T updateColumnBy(I id, String columnName, String value);
}
