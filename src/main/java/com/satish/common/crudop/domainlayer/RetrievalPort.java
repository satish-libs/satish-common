package com.satish.common.crudop.domainlayer;

import java.util.List;
import java.util.Set;

public interface RetrievalPort<T, I>  {
    T findById(I id);

    List<T> findAll();

    Set<T> setOfAll();

}
