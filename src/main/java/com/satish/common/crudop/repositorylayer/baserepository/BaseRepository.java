package com.satish.common.crudop.repositorylayer.baserepository;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.util.Set;

@NoRepositoryBean
public interface BaseRepository<T, I> extends Repository<T, I> {
    T update(T entity);

    T softDelete(I id);

    T updateColumnBy(I id, String columnName, Object value);
}
