package com.satish.common.crudop.repositorylayer.baserepository.impl;

import com.satish.common.crudop.repositorylayer.baserepository.BaseRepository;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Transactional
public class BaseRepositoryImpl<T, I> extends SimpleJpaRepository<T, I> implements BaseRepository<T, I> {

    private final EntityManager entityManager;
    private JpaEntityInformation<T, ?> entityInformation;

    public BaseRepositoryImpl(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.entityManager = entityManager;
        this.entityInformation = entityInformation;
    }

    @Override
    public T update(T entity) {
        if (!this.entityInformation.isNew(entity)) {
            return entityManager.merge(entity);
        }
        return entity;
    }

    @Override
    public T softDelete(I pid) {
        entityManager
                .createQuery("UPDATE " + this.entityInformation.getEntityName() + " SET status = \n" +
                        "  CASE status \n" +
                        "    WHEN TRUE THEN FALSE\n" +
                        "    WHEN FALSE THEN TRUE\n" +
                        "    ELSE status END where id = ?1").setParameter(1, pid)
                .executeUpdate();
        return this.entityManager.find(this.entityInformation.getJavaType(), pid);
    }

    @Override
    public T updateColumnBy(I id, String columnName, Object value) {
        StringBuilder sqlUpdate = new StringBuilder();
        sqlUpdate.append("Update ");
        sqlUpdate.append(this.entityInformation.getEntityName());
        sqlUpdate.append(" SET ");
        sqlUpdate.append(columnName);
        sqlUpdate.append(" = ?1 ");
        sqlUpdate.append("where id = ?2");
        entityManager
                .createQuery(sqlUpdate.toString())
                .setParameter(1, value)
                .setParameter(2, id)
                .executeUpdate();
        return this.entityManager.find(this.entityInformation.getJavaType(), id);
    }
}
