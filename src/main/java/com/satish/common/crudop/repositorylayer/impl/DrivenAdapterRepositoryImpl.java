package com.satish.common.crudop.repositorylayer.impl;

import com.satish.common.crudop.domainlayer.RepositoryPort;
import com.satish.common.crudop.repositorylayer.DrivenAdapterRepository;
import com.satish.common.mapper.generics.CollectionMapper;
import com.satish.common.mapper.generics.SingleMapper;
import com.satish.common.mapper.generics.concrete.CollectionMapperFactory;
import com.satish.common.util.CollectionKey;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class DrivenAdapterRepositoryImpl<T, S, I> implements RepositoryPort<T, I> {
    private final DrivenAdapterRepository<S, I> repository;
    private final SingleMapper<T, S> singleMapper;
    private CollectionMapper<T, S> listMapper;

    public DrivenAdapterRepositoryImpl(final DrivenAdapterRepository<S, I> repository, final SingleMapper<T, S> singleMapper) {
        this.repository = repository;
        this.singleMapper = singleMapper;
        this.listMapper = CollectionMapperFactory.getMapper(CollectionKey.LIST, singleMapper);
    }

    @Override
    public T save(T t) {
        return singleMapper.toTarget(repository.save(singleMapper.toSource(t)));
    }

    @Override
    public T softDelete(I id) {
        return singleMapper.toTarget(repository.softDelete(id));
    }

    @Override
    public T updateColumnBy(I id, String columnName, String value) {
        return singleMapper.toTarget(repository.updateColumnBy(id, columnName, value));
    }

    @Override
    public T findById(I id) {
        return singleMapper.toTarget(repository.findById(id).orElse(null));
    }

    @Override
    public List<T> findAll() {
        return (List<T>) listMapper.toTargets(repository.findAll());
    }

    @Override
    public Set<T> setOfAll() {
        return new LinkedHashSet<>(listMapper.toTargets(repository.findAll()));
    }
}
