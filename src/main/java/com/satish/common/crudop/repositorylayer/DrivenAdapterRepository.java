package com.satish.common.crudop.repositorylayer;

import com.satish.common.crudop.repositorylayer.baserepository.BaseRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DrivenAdapterRepository<T, I> extends BaseRepository<T, I>, JpaRepository<T, I> {
}
