package com.satish.common.mapper.generics;

public interface SingleMapper<T, S> {
    T toTarget(S source);

    S toSource(T target);
}
