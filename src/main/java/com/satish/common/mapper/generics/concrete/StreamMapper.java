package com.satish.common.mapper.generics.concrete;

import com.satish.common.mapper.generics.SingleMapper;

import java.util.Collection;
import java.util.stream.Stream;

public class StreamMapper<T, S> {
    private SingleMapper<T, S> singleMapper;

    public StreamMapper(final SingleMapper<T, S> singleMapper) {
        this.singleMapper = singleMapper;
    }

    public Stream<T> mapToTarget(Collection<S> sources) {
        if (sources.isEmpty()) {
            return Stream.empty();
        }
        return sources.stream()
                .map(singleMapper::toTarget);
    }

    public Stream<S> mapToSources(Collection<T> targets) {
        if (targets.isEmpty()) {
            return Stream.empty();
        }
        return targets.stream()
                .map(singleMapper::toSource);
    }
}