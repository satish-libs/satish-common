package com.satish.common.mapper.generics;
import java.util.Collection;

public interface CollectionMapper<T, S>  {
    Collection<T> toTargets(Collection<S> sources);
    Collection<S> toSources(Collection<T> targets);
}
