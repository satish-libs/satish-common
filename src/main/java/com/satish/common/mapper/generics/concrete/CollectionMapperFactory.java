package com.satish.common.mapper.generics.concrete;

import com.satish.common.mapper.generics.CollectionMapper;
import com.satish.common.mapper.generics.SingleMapper;
import com.satish.common.util.CollectionKey;

public class CollectionMapperFactory {
    private CollectionMapperFactory() {}
    public static <E, M> CollectionMapper<E, M> getMapper(CollectionKey key, SingleMapper<E, M> singleMapper) {
        if (key == null) {
            return null;
        }
        if (key.equals(CollectionKey.SET)) {
            return new SetMapper<>(singleMapper);
        } else if (key.equals(CollectionKey.LIST)) {
            return new ListMapper<>(singleMapper);
        }
        return null;
    }
}
