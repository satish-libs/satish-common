package com.satish.common.mapper.generics;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface AbstractMapper<T, S> extends SingleMapper<T, S> {

    default List<T> toTargetList(List<S> sources) {
        return mapToTarget(sources)
                .collect(Collectors.toList());
    }

    default List<S> toSourceList(List<T> targets) {
        return mapToSource(targets)
                .collect(Collectors.toList());
    }

    default Set<T> toTargetSet(Set<S> sources) {
        return mapToTarget(sources)
                .collect(Collectors.toSet());
    }

    default Set<S> toSourceSet(Set<T> targets) {
        return mapToSource(targets)
                .collect(Collectors.toSet());
    }

    default Set<T> toTargetMap(Set<S> sources) {
        return mapToTarget(sources)
                .collect(Collectors.toSet());
    }

    private Stream<T> mapToTarget(Collection<S> sources) {
        if (sources.isEmpty()) {
            return Stream.empty();
        }
        return sources.stream()
                .map(this::toTarget);
    }

    private Stream<S> mapToSource(Collection<T> targets) {
        if (targets.isEmpty()) {
            return Stream.empty();
        }
        return targets.stream()
                .map(this::toSource);
    }
}
