package com.satish.common.mapper.generics.concrete;

import com.satish.common.mapper.generics.CollectionMapper;
import com.satish.common.mapper.generics.SingleMapper;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

public class SetMapper<T, S> implements CollectionMapper<T, S> {
    private StreamMapper<T, S> streamMapper;
    public SetMapper(final SingleMapper<T, S> singleMapper) {
        streamMapper = new StreamMapper<>(singleMapper);

    }
    @Override
    public Set<T> toTargets(Collection<S> sources) {
        if (sources.isEmpty()) {
            return Collections.emptySet();
        }
        return streamMapper.mapToTarget(sources)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<S> toSources(Collection<T> targets) {
        if (targets.isEmpty()) {
            return Collections.emptySet();
        }
        return  streamMapper.mapToSources(targets)
                .collect(Collectors.toSet());
    }
}
