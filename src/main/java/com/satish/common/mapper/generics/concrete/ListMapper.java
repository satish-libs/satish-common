package com.satish.common.mapper.generics.concrete;

import com.satish.common.mapper.generics.CollectionMapper;
import com.satish.common.mapper.generics.SingleMapper;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ListMapper<T, S> implements CollectionMapper<T, S> {
    private StreamMapper<T, S> streamMapper;
    public ListMapper(final SingleMapper<T, S> singleMapper) {
        streamMapper = new StreamMapper<>(singleMapper);

    }
    @Override
    public List<T> toTargets(Collection<S> sources) {
        if (sources.isEmpty()) {
            return Collections.emptyList();
        }
        return streamMapper.mapToTarget(sources)
                .collect(Collectors.toList());
    }

    @Override
    public List<S> toSources(Collection<T> targets) {
        if (targets.isEmpty()) {
            return Collections.emptyList();
        }
        return streamMapper.mapToSources(targets)
                .collect(Collectors.toList());
    }
}
